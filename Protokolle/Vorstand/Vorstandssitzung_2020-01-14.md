Zeit: 14.01.2020, Start: 19:40 Uhr, Ende: 22:00 Uhr

Ort: Labyrinth, Aachen

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 07.01.2020 per E-Mail


# [TOP 2] Annahme der Protokolle vom 12.11.2019 und 10.12.2019

- Beide Protokolle einstimmig angenommen


# [TOP 3] Mitgliedsanträge

- Einstimmig angenommen


# [TOP 4] Rechenschaftsbericht 2.Halbjahr 2019

- Rechenschaftsbericht (Entwurf) soll im März (vor der MV) fertig sein
- Malte + Sarah fertigen diesen an


# [TOP 5] Sponsoring Marien-Hospital Birkesdorf endet

- Es wurde einstimmig beschlossen, dass der F3N den bisher vom Marien-Hospital
  gesponserte Beitrag iHv 125€/Monat an Relaix übernehmen wird.


# [TOP 6] Kontakt zu NetAachen, RelAix, ComNet

- ComNet kommt ebenfalls in Frage, Angebot muss angefordert werden
- Vergleichsangebot von RelAix zum Vergleich des NetAachen Angebots steht noch
  aus


# [TOP 7] Förderantrag NRW für Server

- Angebote für Server-HW wurden von Arne angefragt
- Sarah fragt beim Land nach, ob man 2 verschiedene Förderanträge stellen kann
  oder ob man 2 Projekte in einen Antrag kombinieren muss
- Dennis stellt den Förderantrag für die Server-HW fertig


# [TOP 8] Unterstützung des FFRL

- der auf der MV2019 abgestimmte Förderbeitrag iHv 50€/Monat wurde im Jahr
  2019 überwiesen


# [TOP 9] Unterstützung des Open Infrastructure Orbit

- 250€ Spende an den Förderverein Freie Netze e.V. für die Spendenkampagne
  36COIO
- Einstimmig beschlossen


# [TOP 10] Ticketsystembefüllung

- Workshop notwendig
- Sarah kümmert sich um die Integration von Facebook in das Ticketsystem


# [TOP 11] Ziele für 2020 besprechen

- keine Ziele festgelegt


# [TOP 12] Termin für Mitgliedervollversammlung (12.03. oder 19.03.)

- 19.03. mit Vorbehalt festgehelt
- Räumlichkeit muss geklärt werden)
  - DigitalChurch
    - Jan fragt morgen nach
  - Chico Mendes


# [TOP 13] Slack Freistellungsbescheinigung

- Aktuell haben wir keinen Non-Profit-Status mehr und sind aktuell im
  kostenpflichtigen Plan gelandet. Aktuell ist in Klärung, ob wir den
  Non-Profit wieder erlangen können.


# [TOP 14] Verschiedenes und Termine

- Themenabend am nächsten Community Treffen (Störerhaftung)
- Kassenprüfung für 2019 ist für den 21.01.2020 geplant
