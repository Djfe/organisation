Zeit: 13.06.2023, Start: 19:35 Uhr, Ende: 20:00 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Dennis
- Matthias
- Stefan
- Florian
- Jan
- Malte

__Abwesend__
- Arne

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 06.06.2023 per E-Mail

# [TOP 1] Annahme Protokoll Vorstandssitzung
- Vorstandssitzung 09.05.2023
  - einstimmig angenommen

# [TOP 2] MV23 Nachbereitung
- Protokoll
  - wurde korrigiert und an Jan übergeben
  - Dennis druckt aus, unterschreibt und gibt dieses dann an Stefan
  - Stefan unterschreibt und gibt dieses dann an Jan
- Notar
  - Jan hat das Protokoll bereits zum Notar gesendet

# [TOP 3] Verschiedenes & Termine
- nicht zahlende Mitglieder
  - nochmal Anschreiben/-rufen und nachfragen
    - Florian bietet sich an
      - bekommt die erforderlichen Daten von Jan
    - am sinnvollsten eine Rundmail an die Mitglieder
  - Schuldenschnitt von 12€/Jahr ist seitens Vorstand ok
  - offene Forderungen verfallen nach 3 Jahre
- Spendenteilnahme Netto
  - aktuell werden die abgegebenen Stimmen ausgewertet
  - Netto sendet nach der Stimmen-Auswertung eine E-Mail, ob man an der
    Spende teilnimmt oder nicht
- E-Mail AMABLE.EU
  - möchte gerne unseren Jitsi-Server nutzen, findet aber keine AGB
  - Statement: unser Jitsi-Server kann frei genutzt werden, wir kontrollieren
               dir Nutzung nicht und garantieren keine Qualität & Verfügbarkeit
    - Florian schreibt die Mail
  - wir sollten mal schauen, ob wir eine Datenschutzerklärung + AGB im Jitsi
    hinterlegen
    - Freifunk München hat da evtl. eine Lösung die man sich angucken kann
    - Datenschutzerklärung auf unserer Homepage überarbeiten
