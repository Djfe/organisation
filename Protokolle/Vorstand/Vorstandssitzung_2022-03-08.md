Zeit: 08.03.2022, Start: 19:40 Uhr, Ende: 20:10 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---


# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 01.03.2022 per E-Mail


# [TOP 2] Annahme Protokoll vom 08.02.2022

- einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen 02/2021

- wurde verschickt
- es gab bereits eine Rückmeldung, dass der offene Betrag von der Stadt Aachen
  überwiesen wird
- Sarah antwortet auf die Rückmeldung, wo nach der aktuellen Kontonummer
  gefragt wird
  - Kontonummer hat sich nicht geändert


# [TOP 4] Mitgliedervollversammlung 2022

- Mail für die Terminplanung wurde noch nicht verschickt
- Arbeitsplanung wie gewohnt


# [TOP 5] Verschiedenes und Termine

- Matthias experimentiert mit einem sicheren Zeitserver
  - Überlegung: sicheren Zeitserver im Freifunk-Netz anbinden

