Zeit: 09.05.2023, Start: 19:35 Uhr, Ende: 20:45 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Dennis
- Matthias
- Stefan
- Florian
- Jan
- Malte

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 02.05.2023 per E-Mail

# [TOP 1] Annahme Protokolle Vorstandssitzung
- Vorstandssitzung 09.08.2022
- Vorstandssitzung 14.03.2023
- Vorstandssitzung 11.04.2023
- Vorstandssitzung 20.04.2023

Alle Protokolle einstimmig angenommen.

# [TOP 2] MV23 Nachbereitung
- Protokoll
  - PR muss noch eingepflegt werden
  - anwesende Mitglieder muss korrigiert werden
  - Dennis schickt eine Benachrichtigung, wenn das Protokoll fertig ist
- Notar
  - müssen einen Vor-Ort-Termin klären
    - der Notar bekommt vorab das Protokoll, danach wird ein Termin gemacht
    - die Unterschriften müssen nicht zur selben Zeit erfolgen
  - Vereinsanschrift ändern
    - Anschrift wird auf Arne's Anschrift geändert
- Änderung Vorstand-Mailverteiler
  - hat Arne erledigt
  - Sarah raus, Stefan+Florian rein
- Änderung gitlab-Berechtigungen
- wo soll das bestehende Inventar dokumentiert werden?
  - Papier-Akten sollten da ebenfalls dokumentiert werden
- Sollen wir die Ansprechpartner der Teams dokumentieren?
  - Aktuell müssen diese aus den Protokollen ermittelt werden.
- Brauchen die Teams eine eigene GO?
  - evtl. eine Gemeinschafts-GO
- Konto
  - neue Bevollmächtigte müssen bei der Bank angegeben werden
  - nicht mehr Bevollmächtigte müssen bei der Bank ausgetragen werden
  - Änderung ist erst nach der Satzungsänderung beim Notar sinnvoll
- nichtzahlende Mitglieder
  - nochmal Anschreiben/-rufen und nachfragen
  - Schuldenschnitt anbieten?
    - lt. Gesetz verjähren diese Beiträge nach 3 Jahren.
  - als ruhende Mitglieder deklarieren
  - juristische Probleme?
  - soll bei der nächsten MV geklärt werden
    - Vorstand muss bis dahin einen Vorschlag erstellen

# [TOP 3] Verschiedenes & Termine
- Spendenteilnahme Stawag
  - laut Stawag sind wir nicht nachhaltig und somit nicht teilnahmeberechtigt
- Spendenteilnahme Netto
  - Stefan bewirbt uns
- Own-/NextCloud o.ä. für Vorstand und ggfs. Vereinsaufgaben
  - es ist nicht sinnvoll diese SW auf den Supernodes laufen zu lassen, weil diese dann beschlagnahmt werden könnten,
    wenn jemand falsche Daten hochlädt, wodurch dann die Infrastruktur nicht mehr verfügbar ist.
  - Arne prüft, was RelAix anbieten kann
  - andere Angebote können ebenfalls vorgeschlagen werden
- Sollen Papier-Akten digitalisiert werden?
    - Vorsicht mit personenbezogenen Daten
    - digitalisierte Akten können in die zukünftige Own-/NextCloud abgelegt werden
- Repository muss man sortiert werden
  - u.a. Rechenschaftsberichte
- Mitgliedsantrag vom 21.04.2023 annehmen
  - einstimmig angenommen
- neue Berechtigungen für Firmware-Update-Signierungen
  - werden über Merge-Requests gelöst
