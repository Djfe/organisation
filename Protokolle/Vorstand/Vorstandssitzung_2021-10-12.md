Zeit: 12.10.2021, Start: 19:47 Uhr, Ende: 20:20 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Arne
- Sarah
- Dennis
- Matthias

__Entschuldigt__
- Jan
- Malte

__Fehlt__

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 07.09.2021 bei der Vorstandssitzung.

# [TOP 2] Annahme des Protokolls vom 14.09.2021

- einstimmig angenommen

# [TOP 3] Rechenschaftsbericht Stadt Aachen

- Sarah hat einen Entwurf erstellt und diesen am 28.09.2021 verteilt.
- Es müssen noch aktuelle Traffic-Grafiken erstellt werden

# [TOP 4] Mitgliedervollversammlung Notar

- Der Antrag beim Notar ist seit dem 11.10.2021 vollständig beim Notar
  unterschrieben und kann nun bearbeitet werden

# [TOP 5] Antrag Welthaus

- die aktuellste Fassung muss nochmal durchgelesen und bewertet werden

# [TOP 6] Verschiedenes und Termine

- CCC-Kongress findet dieses Jahr wieder dezentral statt
- Spendenaufruf Freifunk Rheinland
- Nächste Sitzung voraussichtlich 09.11.2021
- Offene Mitgliedsbeiträge F3N sollten für das laufende JAhr überprüft werden
- Langsam die Präsenz vom F3N (1x Monat?) erhöhen?
  - Vorschlag
    - Offline-Treffen beim Community-Treffen
    - Vorstandstreffen weiterhin online
