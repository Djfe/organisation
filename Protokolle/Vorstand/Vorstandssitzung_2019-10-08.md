Zeit: 2019-10-08, Start: 19:41, Ende: 20:18

Ort: Labyrinth, Aachen

Protokoll: Sarah Beschorner


---

__Anwesend__
- Arne
- Dennis
- Jannic
- Sarah

__Abwesend__
- Malte
- Matthias
---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 01.10.2019 per E-Mail.

# [TOP 2] Annahme des Protokolls vom 10.09.2019

Da noch nicht alle Vorstandsmitglieder das Protokoll vollständig gelesen haben, wird die Annahme des Protokolls auf die nächste Sitzung verschoben.

# [TOP 3] Mitgliedsanträge

Es liegt ein neuer Mitgliedsantrag vor. 
Das neue Mitglied wurde in den Verein mit 4 Stimmen angenommen.

# [TOP 4] Steuererklärung 2018

Die Steuererklärung wurde von der Steuerberaterin bearbeitet. Es wurde per Mail über den Prozess informiert. 
Die Steuererklärung wurde nun eingereicht.
Die Kosten der Steuerberaterin belaufen sich auf 208,61€.
Verantwortliche Person: Jannic

# [TOP 5] Rechenschaftsbericht 1.Halbjahr 2019 erstelle

Auf Nachfrage wurde der Rechenschaftsbericht als ausreichend empfunden. Ein neuer Rechenschaftsbericht kann nun eingereicht werden.
Verantwortliche Personen: Malte & Sarah

# [TOP 6] Ticketsystems

Ticketsystem
Es fand eine Kommunikation zwischen Daniel und Malte statt, jedoch hat Arne noch immer keine Zugangsdaten.
Nochmals bei Malte und Daniel für Zugang nachfragen.
Verantwortliche Person: Arne

# [TOP 7] Kontakt zu NetAachen und RelAix

Es soll nochmals Kontakt zu NetAachen hergestellt werden.
Verantwortliche Person: Jannic

# [TOP 8] Linux Presentation Day

Es soll ein Präsentation von 15 min über Freifunk gehalten werden. Paul hatte die Unterlagen zugesendet bekommen und gibt nun an, dass er den Vortrag nicht halten kann. Dennis und Sarah werden den Votrag übernehmen.
Es werden noch Mitglieder für eine Durchführung von einem Workshop -Router flashen- gesucht.
Jannic gibt an, dass er mitmachen kann.
Es werden noch weiter Mitglieder innerhalb der Community angefragt.
Verantwortliche Personen: Dennis & Sarah

# [TOP 9] Hardwareanschaffung

In 12 Wochen muss der Freifunk seine Hardware in ein neues Rechnzentrum bringen. Um den Umzug zu erleichtern, soll ein weiterer Server angeschafft werden. Arne prüft aktuelle Angebote auf dem Neu- und Gebrauchtmarkt. Diese Liste wird bei der nächsten Sitzung zur Abstimmung freigegeben.
Jannic überprüft das mögliche Budget und teilt dies Arne mit.
Verantwortliche Personen: Jannic & Arne 


# [TOP 10] Verschiedenes
Reisekosten von Mitgliedern zu Veranstaltungen des Vereins (auch
Vorstandssitzungen, Community-Treffen) können unter besonderen
Vorraussetzungen vom Verein erstattet werden. Für das Vorstandsmitglied
Matthias werden für Fahrten nach Aachen 15€ vom Verein gutgeschrieben.
Dieser Antrag wurde 4 stimmig beschlossen.
