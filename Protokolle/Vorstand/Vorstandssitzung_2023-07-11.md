Zeit: 11.07.2023, Start: 19:35 Uhr, Ende: 20:10 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Dennis
- Florian
- Jan
- Malte
- Matthias
- Stefan

__Abwesend__

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 04.07.2023 per E-Mail

# [TOP 1] Annahme Protokoll Vorstandssitzung
- Vorstandssitzung 13.06.2023
  - einstimmig angenommen

# [TOP 2] Stand MV23 Nachbereitung
- Notar
  - Dennis hat unterschriebenes Protokoll an Stefan übergeben
  - Stefan übergibt das Protokoll morgen an Jan
  - Jan wird das unterschriebene Protokoll an den Notar senden
  - Arne + Matthias wollen gemeinsam zum Notar zum Unterschreiben gehen

# [TOP 3] Rechenschaftsbericht
- Arne schreibt morgen den ersten Entwurf und wird diesen dann den Vorstand
  zukommen lassen

# [TOP 4] Verschiedenes und Termine
- PSD VereinsPreis
  - wir möchten uns da wieder bewerben
  - zum Erstellen des Berwerbungsschreibens, sowie Infomaterial für den Preis
    ist ein Treffen zwischen Sarah, Stefan und Felix für August angesetzt
- Steuerbescheid
  - Arne unterschreibt das Formular und sendet dieses dann eingescannt an die
    Steuerberaterin
- Sommerfest "Am Büschel"
  - das Sommerfest findet vom 31.07.-05.08.2023 statt
  - evtl. können wir da einen Freifunk-Stand machen und Workshops anbieten
  - es gibt die Anfrage, ob wir eine "temporäre" kleine Freifunk-Installation
    anbieten können, weil aktuell unklar ist, ob das geplante WLAN so zu Stande
    kommt.
    - Installation von unserer Seite kein Problem
    - wir benötigen einen Uplink für die Freifunk-Installation
      - es ist auch denkbar, dass wir einen temporären DSL-Anschluss aus
        Vereinsmitteln finanzieren
- Freifunk Düren
  - Matthias hat den Schrank im Komm geräumt
  - Freifunk Düren war das letzte Mal vor Corona im Komm
  - Freifunk Düren wird das Komm erstmal nicht mehr nutzen
- Hotel Seeblick
  - Stefan und Florian werden sich mit Herrn Kommer in Verbindung setzen und
    fahren dann wahrscheinlich dort vorbei und flashen die Geräte
  - Alternativ kann Herr Kommer die Geräte nach Dennis schicken und Dennis
    schickt diese geflasht zurück
