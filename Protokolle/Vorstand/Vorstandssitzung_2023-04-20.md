Zeit: 20.04.2023, Start: 18:50 Uhr, Ende: 18:51 Uhr

Ort: RelAix Networks GmbH, Auf der Hüls 172, Aachen

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Dennis
- Matthias

__Abwesend__
- Sarah
- Malte

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 12.04.2023 per E-Mail

# [TOP 1] Annahme Mitgliedsantrag
- einstimmig angenommen
