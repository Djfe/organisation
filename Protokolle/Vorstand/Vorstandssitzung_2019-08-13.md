Zeit: 13.08.2019, Start: 19:50 Uhr, Ende: 21:15 Uhr

Ort: Labyrinth, Aachen

Protokoll: Dennis Crakau

---

__Anwesend__
- Jan
- Sarah
- Malte (bis 20:15, ab 21:00)
- Matthias
- Dennis
- Arne (ab 20:15)

__Abwesend__

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 06.08.2019 bei der Vorstandssitzung.


# [TOP 2] Annahme des Protokolls vom 09.07.2019

- Einstimmig angenommen


# [TOP 3] Steuererklärung 2018

- Es wurde einstimming beschlossen, dass wir einen Steuerberater in Anspruch
  nemen werden.
- Jan kümmert sich darum.


# [TOP 4] Rechenschaftsbericht 1.Halbjahr 2019 erstellen

- Malte und Sarah kümmern sich darum
- Das Ziel ist, dass dieser in diesem Monat an die Sadt Aachen geschickt wird
- Arne hat ein Soll-Freifunk-Netzwerk erstellt, welches NetAachen mit
  einbezieht. Dieses wird noch ins gitlab geladen.


# [TOP 5] Wahl des Ticketsystems

- Einstimming beschlossen, dass das System "Zammad" getestet werden soll.
- Arne richtet ein


# [TOP 6] Kontakt zu NetAachen und RelAix

- Bisher kein Kontakt hergestellt


# [TOP 7] Ehrenwert-Anmeldung und weitere Organisation

- Sarah hat alle wichtigen Info's im Ehrenwert-Slack gepostet
- Ist eine Haftpflicht notwendig?
  - Sarah fragt nach


# [TOP 8] PSD VereinsPreis 2019 - bewerben?

- Nicht bewerben, weil wir nicht gemeinnützig sind


# [TOP 9] Verschiedenes und Termine

- 20.09.2019 enwor Kontaktaufnahme durch Sarah

