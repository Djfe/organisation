Zeit: 08.08.2023, Start: 19:35 Uhr, Ende: 20:45 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Arne

---

__Anwesend__
- Arne
- Dennis
- Jan
- Matthias
- Stefan

__Abwesend__
- Malte
- Florian

---

# [TOP 0] Formalia
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 01.08.2023 per E-Mail

# [TOP 1] Annahme Protokoll Vorstandssitzung
- Vorstandssitzung 11.07.2023
  - einstimmig angenommen

# [TOP 2] Stand MV23 Nachbereitung
- Notar
  - Termin beim Notar am 24.08.23

# [TOP 3] Rechenschaftsbericht
- Arne pushed den Bericht zeitnah ins Git
- Jan und Matthias tragen die Zahlen für H1/2023 nach

# [TOP 4] Verschiedenes und Termine
- PSD VereinsPreis
  - wir möchten uns da wieder bewerben
  - Treffen hat mit Stefan und Felix stattgefunden
  - Stefan und Felix verfolgen das Thema weiter und reichen alles ein
- Sommerfest "Am Büchel"
  - das Sommerfest fand ohne uns statt
  - Stefan läuft seit Wochen einem Termin hinterher
- Hotel Seeblick
  - Stefan und Florian waren vor Ort
  - es existieren 10 Unifi AC Lite, allerdings gibt es keine PoE Injektoren
  - fehlende Hardware wurde beschafft, u.a. Offloader und D-Link X1860 "Steckdosen-Repeater"
  - Termin für die Installation Ende KW33
- Eventbox Düren
  - steht wieder für Tageseinsätze zur Verfügung 
- Serverinfrastruktur
  - Proxmox läuft nun als Cluster
  - Ceph stellt den Storage zur Verfügung
  - Migrationen waren Live möglich, außer Buildserver
    - Images müssen neu signiert werden
- Lousberglauf
  - Freifunk war an Start und Ziel verfügbar
  - einige Hundert Clients waren zwischenzeitlich aktiv 
- DGB Haus
  - LTE Uplink ersetzt den bisherigen Uplink
  - Firmware ist nun up2date
