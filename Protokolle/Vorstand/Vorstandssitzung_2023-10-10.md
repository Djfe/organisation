Zeit: 10.10.2023, Start: 19:50 Uhr, Ende: 20:45 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Dennis
- Matthias
- Malte
- Stefan

__Abwesend__
- Jan
- Florian

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte am 04.10.2023 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 12.09.2023
  - Einstimmig

# [TOP 2] Stand MV Nachbereitung

- Matthias und Arne sind im Vereinsregister eingetragen
- Eintragung liegt vor
- Sobald die Eintragung vorliegt, sollen Matthias und Arne als
  Kontobevollmächtige eingetragen werden
  - Termin wird geklärt, wenn Jan wieder ausm Urlaub ist
  - wird vermutlich nicht mehr im Oktober geändert

# [TOP 3] Richtfunk

- wie ist der Stand?
  - der Herr Albrecht war bisher telefonisch nicht erreichbar
- Stefan versucht den Kontakt mit Herrn Albrecht herzustellen um offene Fragen
  zu klären
  - muss ein richtiges Angebot eingeholt werden oder genügt ein
    Warenkorb-Ausdruck?
    - benötigt man mehrere Angebote?
  - ist noch Geld im Topf?
  - wir wurden das letzte mal darauf angesprochen, dass man doch
    Unkosten/Pauschalen in den Antrag schreiben solle, was wäre denn da ein
    angemessener Wert?

- aktuell sind 3 Strecken geplant
  - Drehturm
    - Welthaus
    - Seilgraben
    - Studentenwohnheim/ die Türme

# [TOP 4] Verschiedenes und Termine

- Rechenschaftsbericht 2023/H1
  - wurde noch nicht versendet
  - es war bereits nur ein OK da

- offene Mitgliedsbeiträge
  - Rundmail mit offenen Beiträgen soll nach Änderung der Kontobevollmächtigten
    erfolgen
  - Rundmail sollte in zukunft regelmäßiger (z.B. halbjährlich) erfolgen

- Fördergeld Stadt Aachen
  - bei der Rückmeldung vom Rechenschaftsbericht erfolgt in der Regel eine
    Meldung, ob der Antrag der Stadt Aachen verlängert wurde
  - https://ratsinfo.aachen.de/bi/vo020.asp?VOLFDNR=27359

- Zugangsdaten Wordpress-Server
  - Zugänge für Felix/Stefan notwendig/gewünscht?
  - Backup von der Homepage ist vorhanden
  - Shiva hat Zugangsdaten zu diesem Server
    - er wartet ebenfalls diesen Server
    - Server hat unattended Updates

- Kaufland Spendenprogramm
  - Stefan bereitet da was vor

- Erwerb einer Lizenz für die Nutzung von BFWA Frequenzen im 5GHz Band von der
  Bundesnetzagentur für den Kreis Aachen
