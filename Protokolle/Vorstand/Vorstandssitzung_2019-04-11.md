Zeit: 11.04.2019, Start: 20:09 Uhr, Ende: 20:10 Uhr

Ort: DIGITAL CHURCH, Jülicher Str. 72a, 52070 Aachen

Protokoll: Dennis Crakau

---

__Anwesend__
- Jan
- Malte
- Dennis
- Gregor
- Arne

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte nicht ordnungsgemäß

# [TOP 2] Mitgliedsantrag

- Der Mitgliedsantrag wurde einstimming angenommen
