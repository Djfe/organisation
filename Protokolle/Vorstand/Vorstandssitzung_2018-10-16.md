Zeit: 16.10.2018, Start: 19:30 Uhr, Ende: 20:22 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor

Dennis fehlt entschuldigt.

---

# [TOP 1] Formalia

- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 9.10.2018 im Slack

# [TOP 2] Versicherung

Es besteht Konsens für die "große" Versicherung (Hiscox), allerdings ist
zunächst sicherzustellen, dass durch den Verein beauftragte Mitglieder und Nichtmitglieder,
die (auch unentgeltlich und ohne Arbeitsvertrag etc.) im Auftrag des Vereins tätig sind, versichert sind.
Dies ist möglichst durch die Versicherung, mindestens durch den Makler, schriftlich zu bestätigen.

Weiterhin wird Rat von Herrn Schmitz Schunken eingeholt.

Sollte ein Abschluß dieser Versicherung nicht zustande kommen, schließt der Vorstand für den
Freifunktag eine Eventversicherung ab.

# [TOP 3] Mitgliedsanträge

Der Antrag von S. wird mit 3 fürstimmen und einer Enthaltung angenommen.

# [TOP 4] Mitgliedschaften

Antrag auf Mitgliedschaft ist dem DigitalHub zugegangen.

# [TOP 5] NetAachen

Ein Angebot von NetAachen wird eingeholt.
Der Vorstand soll die Verhandlungen führen.

# [TOP 6] Protokoll MV

Liegt noch nicht vor

# [TOP 7] Angebot Relaix

Liegt noch nicht vor

# [TOP 8] Finanzsituation

Städtisches Geld für 2019 (10.000€) ist erst ab April auf dem Konto zu erwarten, Liquiditätsfragen in Kommunikation einbauen.

# [TOP 9] Kosten Uplink FFRL

Verfügbarkeit finanzieller Mittel vorausgesetzt beteiligt sich der F3N an den
beim FFRL anfallenden Uplinkkosten anteilsmäßig entsprechend des verursachten Traffics
in den Monaten Oktober - Dezember 2018 bis zu einer Höhe von insgesamt 1000€.
