Zeit: 09.08.2022, Start: 19:40 Uhr, Ende: 19:45 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Dennis

---

# [TOP 1] Formalia

- Einladung erfolgte ordnungsgemäß per Mail am 2.8.2022 von Sarah.
- Die abwesenden Vorstandsmitglieder haben in der Einladung übersehen, dass die
  Vorstandssitzung im Labyrinth stattfinden soll.
- Die Versammlung ist nicht beschlussfähig.
- Mangels Beschlussfähigkeit und wegen Abwesenheit der relevanten
  Personen konnten die geplanten Tagesordnungspunkte nicht behandelt werden.

