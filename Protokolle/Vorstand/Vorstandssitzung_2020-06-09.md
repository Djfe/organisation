Zeit: 09.06.2020, Start: 19:50 Uhr, Ende: 20:30 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---
Anwesend:
- Jan
- Arne
- Sarah
- Malte
- Dennis
- Matthias (ab 19:55Uhr)

Abwesend:
---


# [TOP 1] Formalia

- Vorstand war bis 19:55 unvollständig und anschließend vollständig anwesend
- Einladung erfolgte ordnungsgemäß per Mail durch Sarah am 02.06.2020


# [TOP 2] Annahme des Protokolls vom 12.05.2020

- einstimmig angenommen


# [TOP 3] Rechenschaftsbericht 2.Halbjahr 2019

- wurde mit leichten Verzug versendet (11.05.2020)
- wir haben im Nachgang eine Rechtfertigung verschickt, warum wir bisher nicht
  alle erhaltenen Mittel ausgegeben haben
- es ist unklar, ob wir weiterhin bzw aktuell Foerdermittel der Stadt Aachen
  erhalten


# [TOP 4] Foerderantrag NRW für Server

- ist soweit fertig und muss nur noch abgeschickt werden
- Kontrolle bzgl der Foerdermittel der Stadt Aachen fuer 2020
- Nachkontrolle durch 1 Vorstandsmitglied


# [TOP 5] 1 GBit Anschluss NetAachen

- Aktuelles Angebot liegt außerhalb unserer zur Verfuegung stehenden Mittel
- Unsere Nachfrage bzgl Verguenstigung, weil wir andere Anforderungen als die
  angebotenen haben, ist bisher noch offen


# [TOP 6] Zweites Rechenzentrum  ComNet

- bisher keine Veraenderungen


# [TOP 7] Ablauf und Ideen zur MV

- Wir benoetigen eine Satzungsaenderung, damit die MV in Zukunft weiterhin als
  Online-Veranstaltung stattfinden kann, da dies aktuell nur aufgrund einer
  Sonderregelung durch den Staat moeglich ist.
- Es soll ein Test im Rahmen eines Praesenztreffens stattfinden. Dadurch soll
  festgestellt werden, wie praktikabel eine MV in Kombination von
  Vor-Ort-Versammlung und gleichzeitigen Online-Mitgliedern ist.


# [TOP 8] Verschiedenes und Termine 

- Termine: bisher sind durch die Pandemie bedingt die meisten Termine ausgefallen.
  Es sind bisher keine neuen Termine dazugekommen.
- Mitgliedsantrag: Einstimmig angenommen
- Antrag auf Mitgliedsaustritt: zur Kenntnis genommen
- Die Vorstandssitzung findet in den naechsten zwei Monaten nicht Turnusmaessig
  statt (Sommerpause). Wenn es die Notwendigkeit zu einer Vorstandsversammlung
  gibt, wird zu dieser - wie gewohnt - eingeladen.

