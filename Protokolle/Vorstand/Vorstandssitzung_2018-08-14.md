Zeit: 14.8.2018, Start: 20:05 Uhr, Ende: 22:00 Uhr

Ort: Labyrinth, Aachen

Protokoll: Gregor Bransky

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 14.8.2018 bei der Vorstandssitzung.

# [TOP 2] Nachbesprechung letzte Sitzung Protokoll:

https://gitlab.com/f3n/vorstand/organisation/blob/master/Protokolle/Vorstand/Vorstandssitzung_2018-07-09.txt

- [TOP 1] Relaix Traffic
  - Trafficanalyse nach ISP hat stattgefunden
  - Malte erklärt Gregor Auswertung im Rahmen vom Treffen
- [TOP 2] Mitgliederversammlung 2018
  - siehe TOP 3 heutige Vorstandssitzung
- [TOP 3] Fördergelder von der Stadt
  - Frau Plesch stellt Nachforschungen an.
  - Geld ist noch nicht da, sollte auf dem Weg sein
- [TOP 4] F3N Domains
  - Shiva hat nix gekauft
  - Diskussion per Slack nochmal.
- [TOP 5] Freifunk - Aachen
  - siehe TOP 4 heutige Vorstandssitzung
- [TOP 6] F3N Bei Freifunk Tag
  - Termin mit Versicherung am 29.08. 
- [TOP 7] Ehrenwert
  - siehe Top 6 heutige Vorstandssitzung
- [TOP 8] Nächste Vorstandssitzungen

# [TOP 3] #2 Mitgliederversammlung 2018
- Begrüßung  (mm,gb)
- Formalia (jn,dc)
  - Leitung (gb, x)
  - TO Reihenfolge ändern? (gb, jn)
- Bericht des Vorstands 
  - Bericht (mm,jn)
  - Kasse (jn,mm)
- Bericht der Kassenprüfer (oh +x)
- Entlastung des Vorstands (alle ?)
  - Hoffentlich
- Wahl des Vorstands (alle ?)
  - Alter Vorstand steht zur Wahl
  - Neue Vorstände?
- Berufung von Teams (gb
  - Team Düren (die Dürener)
  - Team Beggendorf (tbd)
  - Team Backbone (@technik)
  - Team Freifunktag (fn, sb)
- Abstimmung über Ordnungen Geschäfts-, Finanz-, Teams -und Beitragsordnungen (gb,jn)
  - Gregor sucht raus bis nächste Woche
- Abstimmung über anzustrebende Mitgliedschaften des Vereins (gb,mm)
  - Mitgliedschaft im digitalHUB Aachen e.V. (iw,gb)
  - Akademische Vereinigung RWTH (gb,mm)
- Förderung der Teilnahme von Mitgliedern an Veranstaltungen (ab, gb)
  - Festes Budget?
- Geplante Aktivitäten:
  - Freifunktag in Aachen (f,gb)
  - Ausbau Beggendorf (re, x)
  - Anschaffung von Servern für den Betrieb von Freifunk Aachen (ab,cb,mm)
  - Förderanträge (gb, ab, re)
- Richtlinien zur Annahme von Fördermitgliedschaften (alle)
- Verschiedenes (Versammlungsleiter)
- Abschluss der Versammlung (Versammlungsleiter)

# [TOP 4] #6 Domain freifunk-aachen.de
- Ist vorbereitet, wartet auf jvdh

# [TOP 5] #15 Umzug Emails @freifunk-aachen.de
- ist fertig vorbereitet, wartet auf TOP 4

# [TOP 6] #8 Ehrenwert Aktionstag der Aachener Vereine
- 500€ waren letztes mal beschlossen.
- FN hat eine Liste mit geplanten Ausgaben geschickt.
  - Steht im Slack.

# [TOP 7] Mitgliedsanträge
- Die beiden vorliegenden Mitgliedsanträge werden einstimmig angenommen

# [TOP 8] Verminderte Beiträge
- Die drei vorliegenden Anträge auf Ermäßigung werden einstimmig angenommen

# [TOP 9] Sonstiges
- Nächste Vorstandssitzung 31.08. "Besprechung Unterlagen" MV 18 Uhr
