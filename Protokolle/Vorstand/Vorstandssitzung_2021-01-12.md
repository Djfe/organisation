Zeit: 12.01.2021, Start: 20:06 Uhr, Ende: 20:56 Uhr

Ort: Online-Meeting

Protokoll: Arne Bayer

---
__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Matthias
- Dennis

__Abwesend__


---

# [TOP 1] Formalia/Annahme Protokoll

- Einladung erfolgte ordnungsgemäß am 01.12.2020 bei der Vorstandssitzung
- Annahme des Protokolls vom 08.12.2020 einstimmig angenommen

# [TOP 2] Austrittserklärung

- Sachverhalt darlegen, satzungsbedingt erst zu Ende des Jahres
- Jan verfasst Mail

# [TOP 3] Rechenschaftsbericht Stadt Aachen

- bisher gabe es noch keine Rückmeldung, daher Status weiter wartend
- Rückfragen wie der weitere Verlauf sein soll

# [TOP 4] Förderung NRW für Server - Abrechnung

- bisher keine Rückmeldung vom Land
- bei Gelegenheit fragt Dennis mal nach
- Malte entwirft Social Media Beitrag

# [TOP 5] Gemeinnützigkeit für Freifunk

- Gemeinnützigkeit sollte verfolgt werden, damit Spenden einfacher eingesammelt werden können
- Jan fragt beim Finanzamt nach, ob unsere Satzung zur Gemeinnützigkeit kompatibel ist
- notwendige Satzungsänderung sollten in der kommenden Mitgliederversammlung beschlossen werden
- alle Vorstandsmitglieder sollen sich über die Gemeinnützigkeit informieren und eine Meinung bilden

# [TOP 6] Mitgliedervollversammlung Q1 2021

- Terminfindung: Präferenz Donnerstag im März

# [TOP 7] Verschiedens und Termine

- voraussichtlich 19. Januar: Folgetermin Land NRW, Förderung Welthausverkabelung und Richtfunk
