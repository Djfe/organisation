Zeit: 10.03.2020, Start: 19:45 Uhr, Ende: 20:30 Uhr

Ort: Labyrinth, Pontstraße 156-158, 52062 Aachen

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Sarah
- Dennis
- Matthias

__Abwesend__
- Malte

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 03.03.2020 per E-Mail


# [TOP 2] Annahme des Protokolls vom 11.02.2020

- Protokoll einstimmig angenommen


# [TOP 3] Rechenschaftsbericht 2.Halbjahr 2019

- 1. Version ist da, aktuelle Zahlen müssen nachgetragen werden, Korrekturlesen
  erwünscht


# [TOP 4] Kontakt zu NetAachen, RelAix, ComNet

- Zusage von ComNet, dass FFAC Hardware unterstellen darf
- Gespräche mit NetAachen laufen


# [TOP 5] Förderantrag NRW für Server

- direkter Ansprechpartner derzeit krank, Vertretung ist uns bekannt
- Förderanträge für andere Projekte können parallel als eigene Anträge gestellt
  werden
- Statistiken werden von Dennis ergänzt
- Angebote werden unmittelbar vor Antragstellung erneut angefragt, weil die
  Preise aufgrund der aktuellen wirtschaftlichen Lage auf der Welt stark
  schwanken


# [TOP 6] Anmeldung Ehrenwert 2020

- Einstimmig dafür
- Sarah kümmert sich um die Anmeldung


# [TOP 7] Mitgliederversammlung

- bis Montag soll die Präsentation, sowie der Vorstandsbericht fertiggestellt
  sein
- die Versammlung kann trotz COVID-19 stattfinden, da das Risiko laut der
  Bewertungstabelle der Stadt Aachen als "mittel" eingestuft wird


# [TOP 8] Verschiedenes und Termine

- Ehrenwert 30.08.2020
- Linux Presentation Day 16.05.2020, 14:00-18:00Uhr, Welthaus

