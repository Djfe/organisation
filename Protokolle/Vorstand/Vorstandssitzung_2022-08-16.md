Zeit: 16.08.2022, Start: 19:45 Uhr, Ende: 20:03 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Malte Möller / MrMM

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah

__Abwesend__
- Dennis

---
# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 09.08.2022 per E-Mail

# [TOP 1] Annahme des Protokolls 

10.05. Einstimmig angenommen
14.06. Einstimmig angenommen

# [TOP 2]  Rechenschaftsbericht 1. HJ 2022

Sarah hat Entwurf online gestellt, Dennis hat positives Feedback gegeben, Jan hat Zahlen eingefügt.
Arne wird noch Traffic Graphen hinzufügen.

Punkt zu Überweisung an FFRL e.V. muss ins 2. HJ verschoben werden.

# [TOP 3]  Anschaffung SSD

Abstimmung erfolgte per E-Mail:

Beschlussantrag: "Für die Server werden 6 SSDs mit je 256GB zu einem
Gesamtpreis von 329,65€ incl. MwSt gekauft."

Einstimmig per E-Mail angenommen, Überweisung ist erfolgt.

# [TOP 4] PSD Bank 

PSD Bank vergibt einen Vereinspreis per online/SMS Abstimming:
 
Die wichtigsten Fakten:  
Alle gemeinnützigen Vereine und Einrichtungen aus unserem Wettbewerbsgebiet können sich anmelden. 
Ab dem 06. September können alle online mitentscheiden. 
Die sieben Vereine mit den meisten Stimmen je Region erhalten Publikumspreise zwischen 500 und 2.000 Euro. 
Unsere Jury prämiert pro Region einen weiteren Verein mit jeweils 1.500 Euro für besonderes Engagement.  

Der Vorstand hat keine Bedenken gegen die Teilnahme.

# [TOP 5] Sonstige

Matthias hat am 28.06. per Slack seinen Austritt aus dem Vorstand und dem Verein erklärt: "Ich lege mit sofortiger Wirkung mein Amt als Beisitzer nieder. Des weiteren trete ich zum nächstmöglichen Zeitpunkt aus dem Verein aus." Der Vorstand nimmt dies mit Bedauern zu Kenntniss und wird Ihm dies bestätigen. Der Austritt aus dem Vorstand erfolgt unmittelbar, der Austritt aus dem Verein zum Jahresende.

Die Satzung lässt zu einen Ausscheiden Vorstand bis zur nächsten VM mit einem Nachrücker aus dem Verein nachzubesetzen. Florian stellt sich zur Verfügung, wir werden dies für die nächste Vorstandssitzung auf die Tagesordnung setzen.

Antrag Jan, Anschaffung TAN Generator für die Vereinskasse um nicht länger den privaten verwenden zu müssen. Kostenpunkt ca. 35€
Einstimmig angenommen.
