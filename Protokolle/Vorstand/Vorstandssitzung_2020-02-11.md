Zeit: 11.02.2020, Start: 19:45 Uhr, Ende: 22:00 Uhr

Ort: Labyrinth, Aachen

Protokoll: Arne

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 04.02.2020 per E-Mail

# [TOP 2] Annahme der Protokolle vom 14.01.2020

- Protokoll einstimmig angenommen

# [TOP 3] Rechenschaftsbericht 2.Halbjahr 2019

- Rechenschaftsbericht (Entwurf) soll im März (vor der MV) fertig sein
- Malte + Sarah fertigen diesen an

# [TOP 4] Kontakt zu NetAachen, RelAix, ComNet

- ComNet kommt ebenfalls in Frage, Rackspace wird gestellt, Faser zu NetAachen anfragen
- Vergleichsangebot von RelAix zum Vergleich des NetAachen Angebots steht noch
  aus
- Arne hält Kontakt

# [TOP 5] Förderantrag NRW für Server

- ist in Arbeit
- Angebote liegen vor

# [TOP 6] Termin für Mitgliedervollversammlung 19.03.

- 19.03.2020
- DigitalChurch
- Sarah lädt ein
- per eMail legen wir die TO fest

# [TOP 7] Verschiedenes und Termine

- Malte hat Anfrage bzgl. Direktverbindung in Düsseldorf zu FFRL gestellt
