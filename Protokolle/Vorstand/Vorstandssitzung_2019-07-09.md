Zeit: 09.07.2019, Start: 19:45 Uhr, Ende: 20:35 Uhr

Ort: Labyrinth, Aachen

Protokoll: Arne Bayer

---

__Anwesend__
- Arne
- Jan
- Malte
- Matthias
- Sarah
- Dennis

---

# [TOP 0] Formalia
- Vorstand vollständig anwesend.
- Einladung erfolgte ordnungsgemäß per Mail am 02.07.2019 durch Sarah.

# [TOP 1] Annahme des Protokolls
- Das Protokoll vom 18.06.2019 wurde einstimmig angenommen. 

# [TOP 2] Rechenschaftsbericht Stadt
- Der Rechenschaftsbericht für 2018 ist am 30. Juni 2019 an die Stadt gesendet. Rückmeldung ausstehend. 

# [TOP 3] Kontakt zu NetAachen und RelAix
- Jan kümmert sich drum. Keine Neuigkeiten.

# [TOP 4] Ehrenwert-Anmeldung
- Rückmeldung erhalten, Anmeldung ist durch. Organisation ausstehend. Fläche vergleichbar zu letztem Mal (3x3 Meter). Standort noch unbekannt. Malte kümmert sich um technische Seite.
- Sarah und Matthias übernehmen die Organisation
- Aufbau leiten Sarah, Dennis und Matthias

# [TOP 5] Schulen mit städtischen Geld mit Freifunk ausstatten
- Interessierte Schulen werden bei der Stadt angefragt und um Kontaktaufnahme gebeten. Sarah wird sich um eine Liste der Schulen kümmern.

# [TOP 6] Verschiedens und Termine
- Erinnerung: 27. Juli ab 15:00 Uhr Community-Grillen, eintragen nicht vergessen
- Einstimmige Annahme Protokolle 15. Januar 2019, sowie 12. März 2019
- Matthias fragt für die Annakirmes temporäre Bandbreite bei UnityMedia an

Nächste Vorstandssitzung voraussichtlich 13.08.2019, 19:30

