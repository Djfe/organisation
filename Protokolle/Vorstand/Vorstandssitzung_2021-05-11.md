Zeit: 11.05.2021, Start: 19:39 Uhr, Ende: 20:27 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Sarah

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 04.05.2021 per E-Mail.


# [TOP 2] Annahme der Protokolle vom 13.04.2021 und 29.04.2021
- Änderungen zum Protokoll 29.04.2021 werden eingefügt
- Protokoll vom 13.04.2021: einstimmig angenommen
- Protokoll vom 29.04.2021: einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen

- Bericht wurde von Sarah am 24.04.2021 verschickt
- bisher noch keine Rückmeldung
- Sarah wird mit dem Bericht für das Halbjahr1/2021 beginnen
- Jan merkt an, das hierbei mögliche Ausbezahlungen der Mittel für 2021 berücksichtigt werden sollen


# [TOP 4] Mitgliedervollversammlung 2021 - Nachbesprechung

- Protokoll wurde von Sarah überarbeitet und zur Korrekturlesung freigegeben
- der Vorstand soll bis zum 12.05 Rückmeldung geben
- nächste Woche wird das Protokoll von Dennis ausgedruckt, unterschrieben und an Jan oder Sarah gesendet
- Sobald das Protokoll Aachen erreicht, wird Jan einen Termin beim Notar machen
- Sarah und Jan werden die benötigten Unterschriften beim Notar absolvieren

# [TOP 5] Antrag Welthaus

- Sarah merkt an, dass sie mit dem Antrag vom Welthaus nicht glücklich ist, da dort Posten (Datenspeicherung, Spesen für geleistet Leistungen von Antragsteller) aufgeführt werden, die nicht direkt mit der Förderung von Freifunk in Bezug stehen.
Desweiteren ist der Antrag sehr auf das Welthaus zugeschnitten und wird nicht in seperate Posten von Stadt und Freifunk unterteilt.
Es ist unklar, welche Posten von der Stadt, Förderung von der Stadt und von der Freifunk-Förderung übernommen werden sollen.
- Sarah fragt Christopher um Hilfe um herauszufinden, was bei der Förderung als sinnvoll erachtet werden kann
- Jedes Vorstandsmitglied soll sich die Anmekrungen im Antrag vom 15.05-16.05.21 anschauen 
- am Montag 17.05.21 sollen zu den Punkten Peter informiert und ein weiteres Gespräch angefragt werden


# [TOP 6] Verschiedenes und Termine 

- es gab eine Anfrage der Grünen um Parks mit Freifunk auszustatten
- Anfrage wurde von Malte entgegen genommen und eine Präsentation bei den Grünen gehalten
- positive Rückmeldung, man versucht parteiübergreifend einen jeweiligen Antrag zu erstellen

- Jan hat eine Anfrage vom Wohnsitz erhalten um diesen mit Freifunk auszustatten



