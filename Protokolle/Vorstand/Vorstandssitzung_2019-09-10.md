Zeit: 2019-09-10, Start: 19:55, Ende: 20:10

Ort: Labyrinth, Aachen

Protokoll: Malte Möller


---

__Anwesend__
- Arne
- Dennis
- Malte
- Matthias
- Sarah

__Abwesend__
- Jan

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte ordnungsgemäß am 03.09.2019 per E-Mail.

# [TOP 2] Annahme des Protokolls vom 13.08.2019


Annahme Protokoll
Einstimmig angenommen

# [TOP 3] Steuererklärung 2018

Steuererklärung 2018
jannic ist mit Steuerberater dran, Termin morgen.

# [TOP 4] Rechenschaftsbericht 1.Halbjahr 2019 erstelle

Rechenschaftsbericht
noch kein Feedback zu 2018, Sarah fragt per Mail nach.
Bericht 2019HJ1 sobald Rückmeldung zu 2018 vorhanden.

# [TOP 5] Ticketsystems

Ticketsystem
Arne braucht VM, ubunut 18.04, Malte kommuniziert mit Daniel zwecks Einrichung.

# [TOP 6] Kontakt zu NetAachen und RelAix

Kontakt netAachen und Relaix
Ist der Urlaubszeit zum Opfer gefallen, jannic möchte sich nun um Termin kümmern.

# [TOP 7] Ehrenwert

Ehrenwert Orga & Versicherung
Wir machen es wie letztes Jahr. Detailplanung in Slack/gdocs
Kleidungsbestellung ist heute abgeschlossen.
Sonstige Materialbestellung machen Sarah, Matthias, Bene diese Woche.

# [TOP 8] Verschiedenes
Paul hat die Vortragsvorlage bekommen, um beim LPD im Welthaus einen Vortrag zu machen.
