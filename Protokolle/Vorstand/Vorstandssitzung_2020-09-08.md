Zeit: 08.09.2020, Start: 19:35 Uhr, Ende: 20:40 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Sarah

---
Anwesend:
- Arne
- Sarah
- Malte
- Dennis
- Matthias (bis 19:50 Uhr)

Abwesend:
- Jan


# [TOP 1] Formalia

- Einladung erfolgte ordnungsgemäß per Mail durch Sarah am 01.09.2020


# [TOP 2] Annahme des Protokolls vom 09.06.2020

- mit 4 Stimmen angenommen


# [TOP 3] Rechenschaftsbericht 1.Halbjahr 2020

- wird in der nächsten Woche von Sarah erstellt
- über Gitlab für alle Vorstandsmitglieder einsehbar


# [TOP 4] Foerderantrag NRW für Server

- Antrag kam am 30.07.2020 mit Anmerkungen zurück
- Antrag wurde von Dennis noch einmal überarbeitet und innerhalb der Vorstandssitzung diskutiert und verbessert
- Aktuelle Angebote zu den Servern werden wiederholt von Arne angefragt und in aktualisierter Form an Dennis gesendet
- Dennis wird den Antrag innerhalb dieser Woche zurückschicken


# [TOP 5] 1 GBit Anschluss NetAachen

- keine Rückmeldungen zu Anfragen
- weitere Anfragen und Bemühungen werden eingstellt


# [TOP 6] Zweites Rechenzentrum ComNet

- sobald die Server Hardware zur Verfügung steht, kann diese auch im Rechenzentrum von ComNet untergebracht werden


# [TOP 7] Verschiedenes und Termine

- Malte gibt an, dass das Welthaus ein Klima Camp in Aachen plant. 
Hierfür kann Freifunk das Netzwerk für diesen Zeitraum zur Verfügung stellen
und dieses so unterstützen. Allgemeine Zustimmung zu diesem Vorschlag.  
