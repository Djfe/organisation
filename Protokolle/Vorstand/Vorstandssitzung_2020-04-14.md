Zeit: 15.04.2020, Start: 19:41 Uhr, Ende: 20:08 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Sarah

---
Anwesend:__Anwesend__
- Jan
- Arne
- Malte
- Matthias
- Dennis
- Sarah

Abwesend:

---

# [TOP 1] Formalia

- Vorstand ist vollständig anwesend
- Einladung erfolgte ordnungsgemäß per Mail durch Sarah Beschorner am 07.04.2020

- Der Antrag auf Beschlussfassung des Vorstands im Umlaufverfahren in der
  Corona Krise wurde am 31.03.2020 an alle Vorstandsmitglieder per Mail gesendet.
  Wortlaut: Der Vorstand des F3N Aachen e.V. beschließt mit sofortiger Wirkung
  die folgende Geschäftsordnung für den Vorstand:
  "Der Vorstand entscheidet in Sitzungen, zu denen jedes Vorstandsmitglied mit
  einer Frist von 6 Tagen - im Einvernehmen aller Vorstandsmitglieder auch
  kürzer - einladen kann.
  Der Vorstand ist beschlussfähig, wenn mindestens die Hälfte der
  Vorstandsmitglieder anwesend ist. Er entscheidet mit einfacher Mehrheit. Die
  Sitzungen können unter Verzicht auf die Versammlung an einem Ort auf
  elektronischem Wege, zum Beispiel in einer Videokonferenz, durchgeführt
  werden, wenn kein Vorstandsmitglied diesem Verfahren widerspricht.
  Weiterhin können Beschlüsse im Umlaufverfahren gefasst werden, wenn mindestens
  2/3 aller Vorstandsmitglieder dem Beschluss zustimmen. Die Ergebnisse einer
  jeden Vorstandssitzung sind durch eine(n) Protokollführer(in) schriftlich
  festzuhalten. Das Sitzungsprotokoll ist von der/dem Protokollführer(in) und
  einer/m weiteren Sitzungsteilnehmer(in) zu unterzeichnen. Mit Ausnahme der
  Regelungen über die Niederschrift kann die Geschäftsordnung auch durch
  einstimmigen Beschluss des Vorstands geändert werden."

  Alle Vorstandsmitglieder haben diesem Antrag per Mail zugestimmt.

- Der Vorstand beschließt per Handzeichen innerhalb der Videokonferenz, dass
  dieser auch in Online-Vorstabndssitzungen beschlussfähig ist.
  Einstimmig angenommen 


# [TOP 2] Annahme des Protokolls vom 10.03.2020

- Einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt 2. Halbjahr 2019

- Status wartend
- Bericht wird noch durch Malte ergänzt


# [TOP 4] Kontakt zu NetAachen, RelAix, ComNet

- Status wartend

- Arne kümmert sich um die Angebote bezüglich der Infrastruktur von ComNet,
  NetAachen, RelAix


# [TOP 5] Förderantrag für Server

- Es fehlen noch aktuelle Statistiken
- Dennis kümmert sich um die fehlenden Graphen
- Arne fragt nochmal kurzfristig die Angebote an 


# [TOP 6] Anmeldung Ehrenwert 2020

- Die Anmeldung wurde am 01.04.2020 durch Sarah im Verwaltungsgebäude abgegeben


# [TOP 7] Verschiedenes und Termine

- Das Welthaus veranstaltet wieder den Linux Presentation Day (LPD) am 16.05.2020
- Der genaue Ablauf und die Durchführung der Veranstaltung wird in einer
  Videokonferenz am Wochenende näher besprochen.
- Malte & Sarah nehmen an dieser Videokonferenz teil und berichten

- Der aktuelle Jitsi-Server des Freifunk Aachens ist zur Zeit frei nutzbar.
- Unter dem Vorbehalt, dass der Traffic von Freifunk Aachen nicht signifikant
  ansteigt, kann der Jitsi-Server vorerst frei zugänglich bleiben. 
- Malte wird zukunftig den Traffic beobachten.

- Durch entsprechenden gesetzliche Regelungen können Mitgliederversammlungen
  von Vereinen innerhalb der Corona Krise auch online durchgeführt werden. 
- Jan möchte diese Möglichkeit für die ausgefallene Mitgliederversammlung im
  März in Betracht ziehen. 
- Matthias möchte vorerst die neusten Entwicklungen und weiteren Beschlüsse der
  Bundesregierung nach Ostern abwarten. 
- Der Vorstand hat einen möglichen Termin für die Mitgliederversammlung
  (Do. 28. Mai 2020) herausgesucht, aber noch nicht beschlossen.
- Es wurde ein einstimmiger Beschluss gefasst, dass nächste Wocher der Termin
  zur Mitgliederversammlung per Mail abgestimmt werden kann.
