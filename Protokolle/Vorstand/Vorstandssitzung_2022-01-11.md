Zeit: 11.01.2022, Start: 19:35 Uhr, Ende: 20:10 Uhr

Ort: Online-Videokonferenz über Jitsi Plattform (https://meet.freifunk-aachen.de/freifunk)

Protokoll: Dennis

---

__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

---


# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 04.01.2022 per E-Mail.


# [TOP 2] Annahme Protokoll vom 09.11.2021

- einstimmig angenommen


# [TOP 3] Rechenschaftsbericht Stadt Aachen 02/2021

- Sarah hat diesen bereits angefangen
- Arne stellt Grafik zur Verfügung
- Jan stellt Zahlen zur Verfügung


# [TOP 4] Sponsoren order weitere Fördermitglieder finden

- die Stadt Aachen gibt uns erst wieder im H2/2022 Geld. Das vorhandene Geld
  sollte knapp bis dahin ausreichen.
- Aachener Bank wurde informiert, dass wir inzwischen Gemeinnützig sind
- Es soll ein Pressebericht geschrieben werden, dass wir nun Gemeinnützig sind
- Es soll auf der Freifunk-Karte kontrolliert werden, ob es größere
  Installationen gibt, welche man bzgl. Spende kontaktieren könnte


# [TOP 5] Verschiedenes und Termine

- Kloster am Lousberg
  - F3N muss an dieser Stelle aktuell nichts unternehmen
- Gebührenbefreiung Transparenzregister
  - wurde bereits von Jan ausgefüllt und verschickt
- nächste Vorstandssitzung findet am 08.02.2022 statt
