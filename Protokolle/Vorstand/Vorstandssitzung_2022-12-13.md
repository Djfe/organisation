Zeit: 13.12.2022, Start: 19:35 Uhr, Ende: 20:00 Uhr

Ort: https://meet.freifunk-aachen.de/Freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Malte
- Sarah
- Dennis

__Abwesend__
- Jan

---

# [TOP 0] Formalia
- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 06.12.2022 per E-Mail

# [TOP 1] Annahme des Protokolls vom 08.11.2022
- Einstimmig angenommen

# [TOP 2] Steuererklärung
- Steuer ist bei der Steuerberaterin
  - die Steuerberaterin teilt dies dem Finanzamt mit
  - daher sollte eine Abgabefrist bis zum 31.08.2023 gelten
    - Jan hält Rücksprache diesbzgl. mit der Steuerberaterin

# [TOP 3]  Verschiedenes und Termine
- Annahme Wiedereinstieg Matthias
  - Prüfen, ob Matthias überhaupt offiziell ausgetreten war
- Sarah möchte neue Freifunk (ohne Zusatz Aachen) drucken lassen
  - hält Rücksprache mit Jan bzgl. Finanzierung
  - evtl. 2 verschiedene Größen (Router + Außenwerbung)
  - sollten beständiger als die Alten sein
    - Matthias schaut, welches Material Düren verwendet hat
- die Lebenshilfe HPZ möchte sich erkenntlich zeigen, kann aber keine Spenden
  verrichten
  - der F3N würde auch HW-Spenden (z.B. SSD) entgegen nehmen
- der aktuelle Mail-Forward von Stefan soll in eine ordentliche E-Mail-Adresse
  umgewandelt werden
  - wir haben Arne beauftragt
- nächste Vorstandssitzung 10.01.2023
