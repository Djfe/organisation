Zeit: 08.12.2020, Start: 19:50 Uhr, Ende: 20:16 Uhr

Ort: Online-Meeting

Protokoll: Sarah Beschorner

---
__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Matthias
- Dennis (Anwesend ab 19:59 Uhr)

__Abwesend__


---

# [TOP 1] Formalia/Annahme Protokoll

- Vorstand (un)vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 01.12.2020 bei der Vorstandssitzung
- Annahme der Protokolle vom 13.10.2020 und 10.11.2020 einstimmig angenommen

# [TOP 2] Rechenschaftsbericht 1.Halbjahr 2020

- Auf die Rückfrage wurde eine weitere Stellungnahme vom Vorstand am 01.12.2020 
an die Stadt Aachen geschickt.
- bisher gabe es noch keine Rückmeldung, daher Status= wartend

# [TOP 3] Förderung NRW für Server

- die Fördergeldsumme wurde auf Nachfrage von Dennis dem Vorstand in der KW49 
nochmals bestätigt
- das Fördergeld hat der F3N e.V. am 08.12.2020 vom Land NRW erhalten
- Server wurden Arne bestellt
- die benötigte Anzahlung wurde von Jan freigegeben

# [TOP 4] Einrichtung Server/Bestellung
 
- die ersten zwei Server werden von Arne zwischen 09.12 -11.12.2020 abgeholt
- die ersten zwei Systeme werden durch Arne und Malte in der KW 51 installiert 
und in Betrieb genommen

# [TOP 5] Verschiedens und Termine

- Matthias begrüsst es, dass Freifunk-Vereine ab Januar 2021 als
  gemeinnützig anerkannt werden können
- Jan möchte darüber abstimmen, ob dieses Jahr auch wieder eine einmalige Zahlung 
  von 600€ an den Freifunk Rheinland e.V. ausgezahlt werden soll, da der Freifunk 
  Aachen die Infrastruktur des Freifunk Rheinlands mitbenutzt. Der Vorschlag wurde 
  angenommen, mit einer Enthaltung.
- Sarah bedankt sich bei allen Vorstandsmitglieder für die gute Zusammenarbeit 
  in diesem Jahr und freut sich auf die weiteren Projekte im nächsten Jahr

