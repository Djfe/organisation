Zeit: 09.03.2021, Start: 19:35 Uhr, Ende: 21:01 Uhr

Ort: Online-Meeting

Protokoll: Malte

---
__Anwesend__
- Jan
- Arne
- Malte
- Sarah
- Dennis
- Matthias

__Abwesend__

---

# [TOP 0] Formalia
- Einladung erfolgte ordnungsgemäß am 02.03.2020 per E-Mail


# [TOP 1] Vortrag zum Antrag Welthaus (Peter Kämmerling) 

- Peter stellt einen Antrag zur Freifunk Ausstattung des Welthauses vor
- Es soll per Richtfunk über den Drehturm zum Rechenzentrum gehen
- Freifunk Access Points am und ums Gebäude
- Interne Server Dienste
- USV Konzept mit PV
- Es sollen Bildungsangebote durch Termial-PCs unterstützt werden
- Antrag wegen seines Volumens auf der MV
- Bildung eines Teams für das Projekt angestrebt das von der MV berufen werden sollte

# [TOP 2] Annahme des Protokolls vom 09.02.2021
- Mit Korrektur Einstimmig angenommen

# [TOP 3] Rechenschaftsbericht Stadt Aachen
- Sarah hat Entwurf geschriben
- Jan hat die Zahlen beigesteuert
- Zahlen von Daniel sollen noch geprüft werden
- Liegt im internen Gitlab unter Entwürfe


# [TOP 4] Gemeinnützigkeit für Freifunk
- Finanzamt ist mit unserer Satzung zufrieden, sofern wir den Passus zur Gemeinnützigkeit hinzufügen.
- Allgemeine Diskussion zur Definition von Freifunk.
- Rechtspfleger beim Amtsgericht haben nach der Erfahrung von Jan wenig mit dem Inhalt der Satzung zu tun. Hier zählen mehr die Formalitäten wie die korrekte Einladung und MV Durchführung. Wir müssen nur einen Standard Satz hinzufügen. Jan wird sofern er die Zeit findet mit dem Rechtspfleger/in kommunizieren.
- Der Vorstand spricht sich einstimmig dafür aus der Einladung zur MV den Punkt Diskussion Gemeinnützigkeit hinzuzufügen und eine Satzungsänderung vorzubereiten um die Gemeinnützigkeit in die Satzung aufzunehmen.

# [TOP 5] Mitgliedervollversammlung 2021

## [TOP 5.1] Termin
- Die Mitgliederversammlung soll am 29.04.2021 20Uhr stattfinden.
- Sarah formuliert die Einladung
- Jan verschickt die Einladung

## [TOP 5.2] Vorbereitung
- Einladung muss spaetestens am 30.03.2021 verschickt werden.
- Die Versammlungsthemen muessen vor der naechsten Vorstandsversammlung
  versendet werden.
- Fuer die Vorbereitung der Einladung wird keine separate Vorstandsversammlung
  abgehalten.
- Die Erstellung der Praesentation wird bei der naechsten Vorstandsversammlung
  festgelegt.


# [TOP 6] Verschiedenes und Termine
- Entsprechend der Minderausgaben passend Geld an Land zurück gezahlt
- Vorliegender Mitgliedsantrag einstimmig angenommen
