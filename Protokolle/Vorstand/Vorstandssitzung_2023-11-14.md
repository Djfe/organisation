Zeit: 14.11.2023, Start: 19:30 Uhr, Ende: 20:17 Uhr

Ort: Jitsi https://meet.freifunk-aachen.de/freifunk

Protokoll: Dennis

---

__Anwesend__
- Arne
- Dennis
- Stefan
- Jan
- Malte

__Abwesend__
- Florian
- Matthias

---

# [TOP 0] Formalia

- Vorstand unvollständig anwesend
- Vorstand beschlussfähig
- Einladung erfolgte ordnungsgemäß am 07.11.2023 per Mail

# [TOP 1] Annahme von Protokollen

- Vorstandssitzung 10.10.2023
  - eine Enthaltung

# [TOP 2] Stand MV Nachbereitung

- unverändert
- Jan geht nächste Woche zur Bank und fragt nach einen Termin und benötigte
  Unterlagen

# [TOP 3] Richtfunk

- Fördertopf wurde für dieses Jahr abgeschafft
  - unklar, ob dieser nächstes Jahr wieder zur Verfügung steht
- andere Fördermöglichkeiten werden aktuell gesucht

# [TOP 4] Verschiedenes und Termine

- offene Mitgliedsbeiträge
  - Jan hat ausstehende Mitgliedsbeiträge auf Basis der Zahlungseingänge
    festgestellt.

- Fördergeld Stadt Aachen
  - Rechenschaftsbericht angekommen + bestätigt
    - Zahlen stimmen mit unseren überein
  - Im nächsten Rechenschaftsbericht erwähnen, wie lange wir mit dem vorhandenen
    Geld überleben um neues Geld zu erhalten.
  - Alternativ-Angebote zum Server-Housing einholen
    - sollte zum nächsten Rechenschaftsbericht passiert sein
    - Anbieter aus der Region mit physikalischen Zugang
    - weiter entferne Anbieter zusätzlich mit km-Pauschale angeben
    - Arne stellt Liste mit Anbieter aus der Region + NRW zusammen
    - Malte fragt Angebote an

- Kaufland Spendenprogramm
  - bisher nichts passiert

- Erwerb einer Lizenz für die Nutzung von BFWA Frequenzen im 5GHz Band von der
  Bundesnetzagentur für den Kreis Aachen
  - bisher nichts passiert
  - sollen Anfragen im Namen von F3N gestellt werden
    - es spricht nichts dagegen
    - Felix fragt mal an

- gitlab
  - Felix und Stefan haben aktuell nur Zugriff auf "privat"
    - Zugriff auf "info" und "organisation" wird zugeteilt

- Anpassungen Homepage freifunk-aachen.de
  - Impressum
    - Hinweis auf Freifunk Rheinland muss entfernt werden
    - Datenschutzhinweise zu Facebook und Twitter können entfernt werden, weil
      wir keine Daten mehr an diese Dienste übermitteln
    - Malte bleibt weiterhin als Verantwortlicher stehen
    - Felix trägt sich als Verantwortlicher für den Inhalt rein

- Kontaktinformationen freifunk-aachen.de Domain
  - Daten müssen aktualisiert werden
    - Adresse wird auf Arne geändert, weil da aktuell die alte Adresse von Jan
      hinterlegt 
  - E-Mail wird auf vorstand@ geändert
  - Telefonnummer bleibt unverändert
