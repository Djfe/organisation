Zeit: 18.06.2019, Start: 19:45 Uhr, Ende: 20:44 Uhr

Ort: Labyrinth, Aachen

Protokoll: Sarah Beschorner

---

__Anwesend__
- Arne
- Jan
- Malte
- Matthias
- Sarah

__Abwesend (entschuldigt)__
- Dennis

---

# [TOP 0] Formalia

- Vorstand vollständig anwesend.
- Einladung erfolgte ordnungsgemäß per Mail am 11.06.2019 durch Sarah.

# [TOP 1] Annahme des Protokolls

- Das Protokoll vom 21.05.2019 wurde einstimmig angenommen. 

# [TOP 2] Stand Eintragung der Beschlüsse der MV 2019

- Die Änderung der MV und das Protokoll wurden von dem Notar eingetragen. Am 18.06.2019 wurde eine Bestätigung der Eintragung an den Verein geschickt. Hiermit sind alle Formalia für die Eintragung des neuen Vorstands abgeschlossen.

# [TOP 3] Rechenschaftsbericht Stadt

- Der Rechenschaftsbericht soll am 23.06.2019 fertiggestellt werden und in der Kalenderwoche 26 an die Stadt geschickt werden. Sarah und Arne kümmern sich um das Layout des Berichts. Inhaltlich soll der BEricht von Malte und Jan noch vervollständigt werden. Es gab vom Vorstand keine Anmerkungen mehr zum ersten Entwurf.
- Für den Rechenschaftsbericht für das 1. Halbjahr 2019 wird auf die Rückmeldung des Rechenchaftsberichts des 2ten Halbjahrs 2018 gewartet, um möglich Form- oder Inhaltsänderungen direkt richtig ergänzen zu können. Angedachter Abgabe-Termin im Juli 2019.

# [TOP 4] Kontakt zu NetAachen und RelAix

- Jan kümmert sich drum. Keine Neuigkeiten.

# [TOP 5] Ehrenwert 29.09
- Anmeldung ging am 12.06.2019 an die Stadt Aachen.

# [TOP 6] FB400 - Kontakt herstellen
- Kontaktaufnahme wird etwas verzögert, da Sarah ab Mitte/Ende Juli erst die benötigte Zeit aufbringen kann. Diese Verzögerung wurde vom Vorstand zur Kenntnis genommen.

# [TOP 7] Sommergrillen
- Das Sommergrillen soll am 27.07 im Studentenwohnheim Halifax stattfinden. Arne hat die Reservierung angemeldet. Sarah und Matthias übernehmen die Organisation und F lädt zur Veranstaltung die Communties aus der Region ein. Anmeldung erfolgt duch ein Pad.

# [TOP 8] Verschiedens und Termine
  

Nächste Vorstandssitzung voraussichtlich 16.07.2019, 19:30

