Nach ausgiebiger Diskussion und Vorarbeit gründete sich am 20.02.2018 die "Fördervereinigung für freie Netzwerke in der Region Aachen e.V.", kurz F3N, im SuperC der RWTH Aachen. Die Gründungsversammlung erteilte dem Vorstand zunächst nur den Auftrag die notwendigen Rechtsgeschäfte zur Gründung des Vereins durchzuführen, insbesondere zur Eintragung im Vereinsregister. Diese Eintragung erfolgte am 07.05.2018.
Im Anschluss fand daraufhin eine außerordentliche Mitgliederversammlung am 11.09.2018 statt.

Der Verein erhält laut Ratsantrag vom 330/17 vom 22.03.2018 städtische Mittel, welche der folgenden Zweckbindung unterliegen:

"Diese Mittel sollen für die Förderung der Aktivitäten des Vereins, insbesondere für Betrieb und Wartung notwendiger Infrastrukur beim Einsatz von Freifunk sowie zur Kostendeckung der Unterstützung der Stadt Aachen bei der Ausleuchtung öffentlicher Einrichtungen dienen."

# Übersicht städtische Mittel 2018

Aufgrund dessen gingen am 30. August 2018 8.000€ auf dem Konto des F3N ein, in 2018 wurde diese wie folgt verwendet:

|Ausgabe Posten|Betrag|
| ----- | -----: |
|Ausgaben für Infrastruktur (Mailserver, Domains)|47,98€|
|Ausgaben abzgl. Einnahmen Freifunktag|1732,80€|
|Ausgaben 'Erstattung Veranstaltungsteilnahmen'|695,41€|
|Ausgaben Internet|952,00€|
|**Summe**|**3428,19€**|

### Ausgaben für Vereins Infrastruktur

Der Verein zahlte in 2018 Entgelte für Mailserver sowie die Domains, die für Mailserver und Infrastruktur notwendig sind, 47,98€.

### Freifunktag 2018 in Aachen

Vom 26. bis 28.10.2018 fand der Freifunktag 2018 in der Digital Church in Aachen statt. Diese Veranstaltungsreihe findet seit 2013 in wechselnden Städten in NRW statt und hat große Bedeutung für den Austausch zwischen Interessierten für das Thema Netzwerke und den Communities von Freifunk. Das Vortragsprogramm ist einzusehen unter:
https://pretalx.freifunktag.de/freifunktag2018/schedule/

Für Technik, Organisation und Bewirtung sind Kosten von 2988,90€ entstanden, denen Einnahemen in Höhe von 1256,10€ aus Schenkungen, Ticket- und T-Shirt-Verkauf gegenüberstehen. Den Fehlbetrag hat der F3N e.V. getragen. Hierfür wurden somit 1732,80€ aus städtischen Mitteln verwendet.

### Veranstaltungsteilnahmen von Mitgliedern

Der F3N hat Mitgliedern, die mit einem eigenen Beitrag am 35. Chaos Communication Congress (35C3) teilgenommen haben, die Unkosten für Anfahrt und Unterkunft erstattet. Der Kongress ist mit 17.000 Teilnehmern aus IT und Hackerzene, weltweit eine der wichtigsten Veranstaltungen seiner Art. Er fand vom 27.-30.12.2018 statt und es wurden Kosten in Höhe von 695,41€ erstattet. Details zu den Aachener Beiträgen finden sich auf der Freifunk Aachen Homepage:
https://freifunk-aachen.de/2018/12/28/freifunker-refreshing-memories-beim-35c3/

### Ausgaben Internet Bandbreite

Die wesentlichen Ausgaben für den Betrieb der Aachener Freifunk Infrastruktur entsteht durch die Durchleitung von Internet Traffic. Seit 2015 stand der Community unentgeltlich Traffic in einem Aachener Rechenzentrum zur Verfügung. Durch geänderte Besitzverhältnisse in diesem RZ, steht diese kostenfreie Option seit 11/2018 nicht mehr zur Verfügung, seitdem wickelt der F3N den gesamten Traffic über Server bei der RelAix Networks GmbH in Aachen ab. Hierfür haben wir in 2018 952€ bezahlt, für 2019 sind Aufgrund der vollständigen Abwicklung über F3N Server höhere Kosten eingeplant. Darüber hinaus wurden in 2018 folgende Kosten für Bandbreite durch von Dritten getragen:

(Hierzu habe ich keine Zahlen.)
 - ...€ von ...
 - ...€ von ...
 - ...€ von ...
 - ...€ von ...

# Planung weitere Mittelverwendung

### Zweites Rechenzentrum

Um weiterhin den Nutzern von Freifunk eine sehr zuverlässiges Netz anbieten zu können, möchte der F3N das Netz mit Standortübergreifender Redundanz absichern. Daher soll auch in Zukunft darauf geachtet werden, dass der F3N in mindesten zwei unabhängigen Rechenzentren seine Infrastruktur betreibt.

Für die Auswahl der Rechenzentren wurde analysiert, aus welchen Endkundennetzen die Datenströme durch unsere Server stammen. Es zeigte sich, dass durch die lokale Aktivität knapp 1/4 der Knoten des Netzwerks über Internetanschlüsse der NetAachen mit den Servern des F3N verbunden sind (Grafik VPN Tunnel einfügen). Diese zeigen sich zudem für 40% des genutzen Datenvolumens verantwortlich (Grafik Traffic einfügen). Aktuell erreicht den F3N dieser Traffic über Internetknoten in Frankfurt (Main) und Düsseldorf. Aus diesen Grund wurde beschlossen, dass lokale Server bei der NetAachen technisch sinnvoll sind. Wir haben dazu Gespräche mit der NetAachen aufgenommen, sofern wir dort tragfähige Kondition aushandeln können, sollen dort nach Möglichkeit zwei Server betrieben werden. Die Anschaffung der Server soll durch Fördergelder des Landes NRW erfolgen.

### Erneuerung Server RelAix

Zwei der drei Server, die bei der RelAix Networks GmbH betreiben werden, sind bereits 2016 in Betrieb und wurden der Community entgeltlos überlassen. Diese sollen nach Möglichkeit im Rahmen des Antrags für Hardware in einem zweiten RZ gegen einen modernen Server ausgetauscht werden. Dieser wird ein vielfaches der Leistung bereitstellen, als die beiden zu ersetzten Server.
